1.1.1:
- Adapt script to match the SIA update to httpS

1.1.0:
- Remove radius from regex
- Add coma to regex
- Update support URL (Gitlab issues)
- Add changelog file on repo
- Add greasy fork url to repo readme
- Align @run-at
- Add good practice `rel` attribute to a href

1.0.0:
- Initial commit