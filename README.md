# NOTAMs GeoHelper

TamperMonkey script that adds Google Maps link to geolocalisation on Notams from http://notamweb.aviation-civile.gouv.fr

Without script             |  With script
:-------------------------:|:-------------------------:
![](Images/without_script.jpg) |  ![](Images/with_script.jpg)



Also available at https://greasyfork.org/en/scripts/439907-notams-geohelper

[Changelog](https://gitlab.com/mdaitn/notams-geohelper/-/blob/main/changelog.txt)

[Report an issue](https://gitlab.com/mdaitn/notams-geohelper/-/issues)


